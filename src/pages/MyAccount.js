import React, { useState, useContext, useEffect } from 'react';
import { Jumbotron, Card, Col, Row} from 'react-bootstrap';
import UserContext from '../UserContext';
import { Redirect } from 'react-router-dom';

// images
import accountName from '../images/account-name.png';
import accountPhone from '../images/account-phone.png';
import accountEmail from '../images/account-email.png';

export default function MyAccount(){

	const [ firstName, setFirstName ] = useState();
	const [ lastName, setLastName ] = useState();
	const [ phone, setPhone ] = useState();
	const [ email, setEmail ] = useState();

	const { user } = useContext(UserContext);

	
	useEffect(() => {

		fetch('https://still-escarpment-44325.herokuapp.com/user/user-details', {
			headers: {
				'Authorization' : `Bearer ${user.accessToken}`,
				'Content-Type': 'application/json'
			}
		})
		.then(res => res.json())
		.then(userData => {
			console.log(userData)

			setFirstName(userData.firstName);
			setLastName(userData.lastName);
			setPhone(userData.phone);
			setEmail(userData.email);
		})
	})

	if (user.isAdmin === true) {
		return <Redirect to="/products" />
	}

	return (

		<Jumbotron className="mt-5 text-light text-center" style={{backgroundColor: '#ffd166' }}>

		<Row>
			<Col>
				<Card style={{backgroundColor: '#023047' }}>
					<Card.Body>
						<img src={ accountName } alt="accountName"  style={{ width:'30%'}} />
						<Card.Title>Full Name</Card.Title>
						<Card.Text>
							<h3>{firstName} {lastName}</h3>
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col>
				<Card style={{backgroundColor: '#023047' }}>
					<Card.Body>
						<img src={ accountPhone } alt="accountName"  style={{ width:'30%'}} />
						<Card.Title>Mobile Number</Card.Title>
						<Card.Text>
							<h3>{phone}</h3>
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col>
				<Card style={{backgroundColor: '#023047' }}>
					<Card.Body>
						<img src={ accountEmail } alt="accountName"  style={{ width:'30%'}} />
						<Card.Title>Email Address</Card.Title>
						<Card.Text>
							<h3>{email}</h3>
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
		

		</Jumbotron>
	)

}