import React, { useState, useContext, useEffect } from 'react';
import UserContext from '../UserContext';
import { Table, Jumbotron } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

export default function MyOrders() {

	const [ myOrders, setMyOrders ] = useState([]);
	const { user } = useContext(UserContext);

	useEffect(() => {

			fetch('https://still-escarpment-44325.herokuapp.com/user/my-orders', {
				headers: {
					'Authorization': `Bearer ${user.accessToken}`
				}
			})
			.then(res => res.json())
			.then(order => {
				console.log(order);

				setMyOrders(order.orders.map(mine => {

					return (
						<tr key={mine.id}>
							<td>{mine.name}</td>
							<td>{mine.purchasedOn}</td>
							<td>{mine.quantity}</td>
							<td>&#8369; {mine.totalAmount}</td>
							<td>{mine.status}</td>
						</tr>
					)
				}))



			});

	}, [user.accessToken])

	if (user.isAdmin === true) {
		return <Redirect to="/products" />
	}

	return (

		<>
			<Jumbotron className="mt-5 course-jumbo text-light">
				<h1 className="text-center mb-5"><strong>My Orders</strong></h1>
				<Table striped bordered hover>
					<thead className="prod-table-head text-center">
						<tr>
							<th>Pizza Flavor</th>
							<th>Purchase Order Date</th>
							<th>Quantity</th>
							<th>Total Amount</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody className="prod-table">
						{myOrders}
					</tbody>
				</Table>
			</Jumbotron>
		</>

	)
}