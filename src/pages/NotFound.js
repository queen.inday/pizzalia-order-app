import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Jumbotron, Row, Col } from 'react-bootstrap';

export default function NotFound(){

	return (

		<Row>
			<Col>
				<Jumbotron className="mt-5 notfound-jumbo">
				<h1 className="notfound">404 Page Not Found</h1>
				<p>Looks like you've followed a broken link or entered a URL that does not exist on this site</p>
				<Button variant="danger" as={Link} to="/">Back to our site</Button>
				</Jumbotron>
			</Col>
		</Row>

	)
}