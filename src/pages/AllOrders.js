import React, { useState, useContext, useEffect } from 'react';
import UserContext from '../UserContext';
import { Table, Jumbotron } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

export default function AllOrders() {

	const [ allOrders, setAllOrders ] = useState([]);
	const { user } = useContext(UserContext);

	useEffect(() => {

			fetch('https://still-escarpment-44325.herokuapp.com/user/orders', {
				headers: {
					'Authorization': `Bearer ${user.accessToken}`
				}
			})
			.then(res => res.json())
			.then(orders => {
				console.log(orders);

				setAllOrders(orders.map(customerOrders => { 

						return (
						<tr key={customerOrders.id}>
							<td>{customerOrders.firstName}</td>
							<td>{customerOrders.lastName}</td>
							<td>{customerOrders.name}</td>
							<td>{customerOrders.quantity}</td>
							<td>&#8369; {customerOrders.totalAmount}</td>
							<td>{customerOrders.purchasedOn}</td>
						</tr>
					)

				}))

			});

	}, [user.accessToken])

	if (!user.isAdmin) {
		return <Redirect to="/" />
	}

	return (

		<>
			<Jumbotron className="mt-5 course-jumbo text-light">
				<h1 className="text-center mb-5"><strong>All Orders</strong></h1>
				<Table striped bordered hover>
					<thead className="prod-table-head text-center">
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Pizza Flavor</th>
							<th>Quantity</th>
							<th>Total Amount</th>
							<th>Purchase Order Date</th>
						</tr>
					</thead>
					<tbody className="prod-table">
						{allOrders}
					</tbody>
				</Table>
			</Jumbotron>
		</>

		)
}