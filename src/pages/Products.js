import React, { useContext, useState, useEffect } from 'react';
import CustomerProduct from '../components/CustomerProduct';

import UserContext from '../UserContext';
import { Table, Jumbotron } from 'react-bootstrap';
import UpdateButton from '../components/UpdateButton';
import ArchiveButton from '../components/ArchiveButton';
import ReactivateButton from '../components/ReactivateButton';

export default function Products() {

	/*const [ adminProducts, setAdminProducts ] = useState([]);
	const [ allProducts, setAllProducts ] = useState([]);*/
	const [ products, setProducts ] = useState([]);

	const { user } = useContext(UserContext);
	console.log(user);
	console.log(user.accessToken);

	useEffect(() => {
		if (products.length === 0) {
			let headers = { 
				'Content-Type': 'application/json'
			}

			if (typeof user.accessToken !== "undefined" && user.accessToken !== null) {
				headers.Authorization = `Bearer ${user.accessToken}`;
			}
			fetch('https://still-escarpment-44325.herokuapp.com/products/available-products', {
				headers: headers
			})
			.then(res => res.json())
			.then(products => {
				console.log(products);
				setProducts(products);
			});
		}
	}, [user.accessToken, products]);

	// UI conditional
	if(!user.isAdmin || user.accessToken === null){
		return (
			<>
				{
					products.map(product => (
						<CustomerProduct key={product._id} productInfo={product} productId={product._id} />
					))
				}
			</>
		)

	} else {
		return (
			<>
				<Jumbotron className="mt-5 course-jumbo">
					<h1 className="text-center mb-5 text-light"><strong>Administrative Dashboard</strong></h1>
					<Table striped bordered hover>
						<thead className="prod-table-head text-center" >
							<tr>
								<th>Product</th>
								<th>Description</th>
								<th>Price</th>
								<th>Status</th>
								<th colSpan="3">Actions</th>
							</tr>
						</thead>
						<tbody className="prod-table">
							{
								products.map(product => (
									<tr key={product._id}>
										<td>{product.name}</td>
										<td>{product.description}</td>
										<td>&#8369;{product.price}</td>
										<td className={product.isActive ? "text-success" : "text-danger"}>{product.isActive ? "Available" : "Unavailable"}</td>
										<td><UpdateButton productId={product._id} /></td>
										<td><ArchiveButton productId={product._id} /></td>
										<td><ReactivateButton productId={product._id} /></td>
									</tr>
								))
							}
						</tbody>
					</Table>
				</Jumbotron>
			</>
		)
	}
	
}