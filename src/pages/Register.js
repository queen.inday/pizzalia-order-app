import React, { useContext, useState, useEffect } from 'react';
import { Form, Button, Jumbotron } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Redirect, useHistory } from 'react-router-dom';

export default function Register() {

	const history = useHistory();
	const { user } = useContext(UserContext);

	// form inputs
	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ phone, setPhone ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ confirmPassword, setConfirmPassword ] = useState('');

	// submit button
	const [ registerBtn, setRegisterBtn ] = useState(false);

	// submit button conditional rendering with useEffect()
	useEffect(() => {
		if ((firstName !== '' && lastName !== '' && email !== '' && password !== '' && phone !== '' && confirmPassword !== '') && (phone.length === 11) && (password === confirmPassword)){

				setRegisterBtn(true);

		} else {

				setRegisterBtn(false);
		}

	}, [firstName, lastName, phone, email, password, confirmPassword])

	// function to fetch from backend
	function registerUser(e) {

		e.preventDefault();

		fetch('https://still-escarpment-44325.herokuapp.com/user/signup', {
			method: 'POST',
			headers: {'Content-type': 'application/json'},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				phone: phone,
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(reg => {
			console.log(reg);

			Swal.fire({
				title: 'Congratulations!',
				icon: 'success',
				text: 'Your account has been created.',
				confirmButtonText: 'Click to Sign in <i class="fa fa-arrow-right"></i>'
			})

			history.push('/login') // redirect to login page
		})

		// clear out initial input values
		setFirstName('');
		setLastName('');
		setPhone('');
		setEmail('');
		setPassword('');
		setConfirmPassword('');

	}


	// conditional redenring to redirect the user to homepage if accessToken is not empty
	if (user.accessToken !== null){
		return <Redirect to="/" />
	}

	// user interface for registration
	return (

		<>	
		
			<Jumbotron className="mt-5 mr-auto jumbo-reg" style={{backgroundColor: '#ffd166', color: 'black'}}>
				<h2 className="create-account-txt text-center">Create Account</h2>

				<Form className="mt-4" onSubmit={(e) => registerUser(e)}>

					<Form.Group controlId="fName">
						<Form.Label>First Name</Form.Label>
						<Form.Control type="text" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="lName">
						<Form.Label>Last Name</Form.Label>
						<Form.Control type="text" value={lastName} onChange={e => setLastName(e.target.value)} required />
					</Form.Group>

					<Form.Group controlId="phoneNo">
						<Form.Label>Mobile Number</Form.Label>
						<Form.Control type="tel" value={phone} onChange={e => setPhone(e.target.value)} required />
					</Form.Group>

					<Form.Group controlId="emailAdd">
						<Form.Label>Email Address</Form.Label>
						<Form.Control type="email" value={email} onChange={e => setEmail(e.target.value)} required />

						<Form.Text className="text-muted">
							We'll never share your email with anyone else.
						</Form.Text>
					</Form.Group>

					<Form.Group controlId="password1">
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" value={password} onChange={e => setPassword(e.target.value)} required />
					</Form.Group>

					<Form.Group controlId="password2">
						<Form.Label>Confirm Password</Form.Label>
						<Form.Control type="password" value={confirmPassword} onChange={e => setConfirmPassword(e.target.value)} required />
					</Form.Group>

					{ registerBtn ? 
						<Button className="reg-button mt-4" type="submit" block>Submit</Button> : 
						<Button className="reg-button mt-4" type="submit" block disabled>Submit</Button> }
					
				</Form>
			</Jumbotron>
		
		</>
	)
}