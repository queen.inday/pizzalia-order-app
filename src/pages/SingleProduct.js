import React, { useContext, useState, useEffect } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

import { Form, Jumbotron, Button } from 'react-bootstrap';

import { Redirect, Link, useParams, useHistory } from 'react-router-dom';

// import SingleProd from '../components/SingleProd';
// import CustomerProduct from '../components/CustomerProduct';

export default function SingleProduct(productInfo, productId) { 

	const { id } = useParams();
	const { user } = useContext(UserContext);
	// console.log(user);

	const history = useHistory();

	const { name, description, price } = productInfo;

	let [ prodBtn, setProdBtn ] = useState();

	const [ prodName, setProdName ] = useState();
	const [ prodDescription, setProdDescription ] = useState();
	const [ prodPrice, setProdPrice ] = useState();
	const [ prodQty, setProdQty ] = useState(0);

	
	// fetch
	useEffect(() => {
		let headers = { 
			'Content-Type': 'application/json'
		}
		if (typeof user.accessToken !== "undefined" && user.accessToken !== null) {
				headers.Authorization = `Bearer ${user.accessToken}`;
		}
		fetch(`https://still-escarpment-44325.herokuapp.com/products/${id}`, {
			headers: headers
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			
			// get the data
			setProdName(data.name);
			setProdDescription(data.description);
			setProdPrice(data.price)
		})
	}, [id, user.accessToken]);

	// =======================================
	// order fetch

	function placeOrder(e) {

		fetch('https://still-escarpment-44325.herokuapp.com/user/checkout', {
			method: 'POST',
			headers: {
				'Authorization': `Bearer ${user.accessToken}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				productId: id,
				name: prodName,
				price: prodPrice,
				quantity: prodQty
			})
		})
		.then(res => res.json())
		.then(checkout => {
			console.log(checkout);

			Swal.fire({
				title: 'Thank you',
				icon: 'success',
				text: `Your order of ${prodQty} piece/s of ${prodName} has been placed. You will receive a call to confirm your order/s shortly. `,
				footer: 'For more orders, choose and click order now.'
			});

			(history.push('/products'));	
		})
		
		setProdQty(0);


	}
	// =======================================
	
	// conditional rendering for admin not to access this
	if (user.isAdmin) {
		return <Redirect to="/products" />
	}


	return (

	<>
		<Jumbotron className="mt-5 mb-5 jumbo-single-prod">
			
			<Form className="mb-5">
			<h1 className="single-prod-title">{ prodName }</h1>

				<div>
					<span>{ prodDescription }</span>
				</div>
				<div className="mt-3"><p>Price: &#8369;
					<span>{ prodPrice }</span>
				</p></div>


			
			<Form.Label><strong>Quantity</strong></Form.Label>
		    <Form.Control type="number" className="input-number mb-4" placeholder="0" min="1" max="100" value={prodQty} onChange={e => setProdQty(e.target.value)}/>
		   	</Form>

		    { prodBtn = user.accessToken === null 
		    	? 
		    	<Button className="mt-5" className="single-prod-btn" as={Link} to="/login">Sign in to Order</Button>
		    	: 
		    	<Button className="mt-5" className="single-prod-btn" onClick={placeOrder}>Order Now!</Button>
		    }
		</Jumbotron>
	</>

	)
}
