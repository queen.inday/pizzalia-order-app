import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Form, Jumbotron, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect, Link } from 'react-router-dom';

export default function AddProduct() {

	const { user } = useContext(UserContext);
	// console.log(user);

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ addProdBtn, setAddProdBtn ] = useState(false);

	// conditional rendering for add button
	useEffect(() => {

		if (name !== '' && description !== '' && price !== 0) {
			setAddProdBtn(true);

		} else {
			setAddProdBtn(false);
		}
	}, [ name, description, price ])


	// function too add new product
	function addProduct(e) {

		e.preventDefault();

		fetch('https://still-escarpment-44325.herokuapp.com/products/add-product', {
			method: 'POST',
			headers: {
				'Authorization': `Bearer ${localStorage.accessToken}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})

		})
		.then(res => res.json())
		.then(newProd => {

			console.log(newProd);
			console.log(user.accessToken);

			Swal.fire({
				title: 'Wo hoo!',
				icon: 'success',
				text: `${name} has been added to the menu.`
			})
		})

		// clear out initial inputs
		setName('');
		setDescription('');
		setPrice(0);

	}

	// if not admin, redirect to home page and should not access add product endpoint
	if(user.isAdmin === false) {
                return <Redirect to='/' />
	}


	// add product UI
	return (

		<>
			<Jumbotron className="mt-5" style={{backgroundColor: '#ffd166', color: '#023047'}}>
				<h1>Add New Pizza Flavor</h1>

				<Form className="mt-5" onSubmit={(e) => addProduct(e)}>
					<Form.Group>
						<Form.Label>Pizza Name</Form.Label>
						<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
					</Form.Group>

					<Form.Group>
						<Form.Label>Description</Form.Label>
						<Form.Control type="text" as="textarea" value={description} onChange={e => setDescription(e.target.value)} required />
					</Form.Group>

					<Form.Group>
						<Form.Label>Price</Form.Label>
						<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required />
					</Form.Group>

					{addProdBtn ? <Button type="submit" className="mt-4 add-prod">Add</Button> : <Button type="submit" className="mt-4 add-prod" disabled>Add</Button>}
				</Form>

			</Jumbotron>

			<Button className="mt-3 view-dashboard btn-lg" as={Link} to="/products">View Dashboard</Button>
		</>
	)
}