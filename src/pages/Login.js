import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Jumbotron, Col, Row } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Redirect, useHistory, Link } from 'react-router-dom';
import loginImage from '../images/logo-image-pizza.png';

export default function Login() {

	const history = useHistory();
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState(''); // use onChange to be able to input in Form.Control
	const [password, setPassword] = useState('');
	const [LoginBtn, setLoginBtn] = useState(false);

	useEffect(() => {

		if (email !== '' && password !== ''){
			setLoginBtn(true);
		} else {
			setLoginBtn(false);
		}

	}, [email, password]);

	function login(e) {

		e.preventDefault();

		// fetch users/login
		fetch('https://still-escarpment-44325.herokuapp.com/user/login', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(log => {

			console.log(log);

			if (log.accessToken !== undefined) {

				localStorage.setItem('accessToken', log.accessToken);
				setUser({ accessToken: log.accessToken });

				Swal.fire({
				title: "Welcome to Pizzalia!",
				icon: "success",

				});

				fetch('https://still-escarpment-44325.herokuapp.com/user/user-details', {
					headers: {
						'Authorization': `Bearer ${log.accessToken}`
					}
				})
				.then(res => res.json())
				.then(info => {

					console.log(info);

					if (info.isAdmin === true) {

						localStorage.setItem('email', info.email);
						localStorage.setItem('isAdmin', info.isAdmin);

						setUser({
						    accessToken: log.accessToken,
							email: info.email,
							isAdmin: info.isAdmin
						})

						history.push('/products'); // redirect admin to product page

					} else {

						history.push('/'); // redirect non-admin to homepage
					}
				})

			} else {
				Swal.fire({
				title: "Ooooops!",
				icon: "error",
				text: "Something went wrong. Please try again."
				});
			}
		})

		// clearout inputs
		setEmail('');
		setPassword('');
	}	

	// conditional redenring to redirect the user to homepage is accessToken is not empty
	if (user.accessToken !== null){
		return <Redirect to="/" />
	}

	// user interface for login
	return (
		<>
		<Jumbotron className="mt-5 jumbo-login text-center">
			<Row>
				<Col>
				<img src={ loginImage } alt="loginImage"  style={{       
     				 width:'30%'}} />
				<h2 className="login-text">Sign in to Pizzalia</h2>
					<Form className="mt-3 mb-4" onSubmit={(e) => login(e)}>
						<Form.Group controlId="userEmail">
							<Form.Control type="email" placeholder="email address" value={email} onChange={e => setEmail(e.target.value)} required />
						</Form.Group>

						<Form.Group controlId="userPassword">
							<Form.Control type="password" placeholder="password" value={password} onChange={e => setPassword(e.target.value)} required />
						</Form.Group>

						{LoginBtn ? <Button type="submit" className="login-btn mt-3" block>Login</Button> : <Button type="submit" className="login-btn mt-3" disabled block>Login</Button>}
					</Form>

					<Button variant="dark" className="no-acct" as={Link} to="/register">No Account? Create Now!</Button>
				</Col>
			</Row>
		</Jumbotron>

			
		</>
	)
}

