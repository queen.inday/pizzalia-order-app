import React from 'react';
import CarouselSlide from '../components/CarouselSlide';
import Featured from '../components/Featured';
import { Button, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom';

export default function Home(){

	return (
		<>	
			<Container className="text-center">
				
				<CarouselSlide />
				<Featured />
				<Button as={Link} to="/products" className="mt-2 mb-5 home-button" size="lg">See Menu</Button>

			</Container>
			

		</>
	)
}