import React, { useState, useContext, useEffect } from 'react';
import UserContext from '../UserContext';
import { Table, Jumbotron } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

export default function AllUsers() {

	const [ users, setUsers ] = useState([]);
	const { user } = useContext(UserContext);

	useEffect(() => {

			fetch('https://still-escarpment-44325.herokuapp.com/user/get-all-user-roles', {
				headers: {
					'Authorization': `Bearer ${user.accessToken}`
				}
			})
			.then(res => res.json())
			.then(users => {
				console.log(users);

				setUsers(users.map(userRoles => { 

						return (
						<tr key={userRoles.id}>
							<td>{userRoles.firstName}</td>
							<td>{userRoles.lastName}</td>
							<td>{userRoles.phone}</td>
							<td>{userRoles.email}</td>
							<td className={userRoles.isAdmin ? "text-success" : "text-secondary"}>
							{userRoles.isAdmin ? "Administration" : "Customer"}</td>
						</tr>
					)

				}))

			});

	}, [user.accessToken])

	if (!user.isAdmin) {
		return <Redirect to="/" />
	}

	return (
		<>
		<Jumbotron className="mt-5 course-jumbo text-light">
				<h1 className="text-center mb-5"><strong>List of Users</strong></h1>
				<Table striped bordered hover>
					<thead className="prod-table-head text-center">
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Mobile Number</th>
							<th>Email Address</th>
							<th>Role</th>
						</tr>
					</thead>
					<tbody className="prod-table">
						{users}
					</tbody>
				</Table>
			</Jumbotron>
		</>
	)
}