import React, { useState } from 'react';
import './App.css';
import { Container } from 'react-bootstrap';
import UserContext from './UserContext';
// import background from './images/web-bg.jpg';


// import react router dom
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

// components
import NavBar from './components/NavBar';

// pages
import Register from './pages/Register';
import Login from './pages/Login';
import Home from './pages/Home';
import Products from './pages/Products';
import AddProduct from './pages/AddProduct';
import SingleProduct from './pages/SingleProduct';
import NotFound from './pages/NotFound';
import AllOrders from './pages/AllOrders';
import MyOrders from './pages/MyOrders';
import MyAccount from './pages/MyAccount';
import AllUsers from './pages/AllUsers';

function App() {

  const [ user, setUser ] = useState({
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

   const unsetUser = () => {
    localStorage.clear(); // clear localStorage
    setUser({
        accessToken: null,
        isAdmin: null
    }); // set email to null ==> now accessToken
  }
  // productId route 
  return (

    <UserContext.Provider value={{ user, setUser, unsetUser }}>

    <div className='bg'>
      <Router>
        <NavBar />

          <Container>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/products" component={Products} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/addProduct" component={AddProduct} />
                <Route path="/product/:id" component={SingleProduct} />
                <Route exact path="/allOrders" component={AllOrders} />
                <Route exact path="/myOrders" component={MyOrders} />
                <Route exact path="/myAccount" component={MyAccount} />
                <Route exact path="/allUsers" component={AllUsers} />
                <Route component={NotFound} />
            </Switch>
          </Container>

      </Router>
      </div>

    </UserContext.Provider>
  )
}

export default App;
