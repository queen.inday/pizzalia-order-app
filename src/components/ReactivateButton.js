import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveButton({productId}) {

	function activate(e) {

		e.preventDefault();

		fetch(`https://still-escarpment-44325.herokuapp.com/products/activate/${productId}`, {
			// use props to pass the data to string literal
			method: 'PUT',
			headers: {
					'Authorization': `Bearer ${localStorage.getItem('accessToken')}`,
					'Content-Type': 'application/json'
				}
			})
			.then(res => res.json())
			.then(prod => {
				console.log(prod); // return boolean

				if (prod === true) {
					Swal.fire({
						icon: 'success',
						text: 'Product is now available in the menu.'
					}).then(confirm => {
						if(confirm) {
							window.location.reload();
						}
					})

				} else {
					Swal.fire({
						icon: 'info',
						text: 'Product is already available in the menu.'
					})
				}
			})
		
}	


	return (
		<Button variant="success" onClick={activate}>Enable</Button>
	)
}