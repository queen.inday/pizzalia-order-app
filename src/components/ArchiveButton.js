import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveButton({productId}) {

	function deleteProduct(e) {

		e.preventDefault();

		fetch(`https://still-escarpment-44325.herokuapp.com/products/archive/${productId}`, {
			// use props to pass the data to string literal
			method: 'PUT',
			headers: {
					'Authorization': `Bearer ${localStorage.getItem('accessToken')}`,
					'Content-Type': 'application/json'
				}
			})
			.then(res => res.json())
			.then(prod => {
				console.log(prod); // return boolean

				if (prod === true) {
					Swal.fire({
						icon: 'success',
						text: 'Product archived and cannot be seen by customers.'
					}).then(confirm => {
						if(confirm) {
							window.location.reload();
						}
					})
				} else {
					Swal.fire({
						icon: 'info',
						text: 'Product is already removed from customers menu.'
					})
				}
			})
		
	}


	return (

		<Button variant="danger" onClick={deleteProduct}>Archive</Button>
	)
}