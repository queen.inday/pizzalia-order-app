import React from 'react';
import { Card, Row, Col, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

// add PropTypes in the Course component to validate its props
import PropTypes from 'prop-types';


export default function CustomerProduct({ productInfo, productId }){

	const history = useHistory();

	//const userContext = useContext(UserContext);
	// const { user } = userContext;
	const { name, description, price } = productInfo;

	// UI
	return (
		
		<Row>
			<Col>
				<Card className="mt-3 mb-3 card-bg" style={{backgroundColor: '#ffd166', color: '#023047'}}>
					<Card.Body>
						<Card.Title><h4>{ name }</h4></Card.Title>
						<Card.Text>
							<div>
								<span><strong>Description: </strong></span>
								<span>{ description }</span>
							</div>

							<div>
								<span><strong>Price: </strong></span>
								<span>&#8369; { price }</span>
							</div>
						</Card.Text>

						<Button onClick={() => {history.push(`product/${productId}`)}} style={{backgroundColor: '#023047', color: 'white'}}>Details</Button>

					</Card.Body>
				</Card>
			</Col>
		</Row>	
	)
}


// prototypes
CustomerProduct.propTypes = {

	productInfo: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}