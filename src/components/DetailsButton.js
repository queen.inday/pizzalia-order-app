import React, { useContext } from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import UserContext from '../UserContext';
import SingleProd from '../components/SingleProd';

export default function DetailsButton({productId}) {

	const history = useHistory();
	const { user } = useContext(UserContext);

	function getProduct(e) {

		e.preventDefault();

		fetch(`https://still-escarpment-44325.herokuapp.com/products/${productId}`, {
			// use props to pass the data to string literal
			headers: {
					'Authorization': `Bearer ${localStorage.getItem('accessToken')}`,
					'Content-Type': 'application/json'
				}
			})
			.then(res => res.json())
			.then(prod => {
				console.log(prod); // return boolean
				console.log(user.accessToken);
				
				prod.map((product => {
						<SingleProd key={product._id} productInfo={product} productId={product._id} />
				}))

				history.push("/product-details")
			})
		
	}	


	return (

		<Button variant="warning" onClick={getProduct}>Details</Button>
	)
}