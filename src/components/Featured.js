import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import ribbon from '../images/best-seller-ribbon.png';

export default function Highlights(){

	return (

		<>
			<div>

				<img className="mt-4" alt="best-seller" src={ ribbon } style={{ image: `url(${ribbon})`,
				      width:'30%'
				    }}/>


				<Row className="mt-5 mb-5">
					<Col>
						<Card className="cardHighlight card-bg-featured">
							<Card.Body>
								<Card.Title>
									<h2>Pizza Madness</h2>
								</Card.Title>
								<Card.Text>
									All-time favorite takes more than just topping the pizza with all the flavorsome stuff and popping it in the oven.
								</Card.Text>
							</Card.Body>
						</Card>
					</Col> 

					<Col>
						<Card className="cardHighlight card-bg-featured">
							<Card.Body>
								<Card.Title>
									<h2>Creamy Garlic and 5 cheese</h2>
								</Card.Title>
								<Card.Text>
									Creamy pizza with fried garlic on top and 5 different kinds of cheese.
								</Card.Text>
							</Card.Body>
						</Card>
					</Col>

					<Col>
						<Card className="cardHighlight card-bg-featured">
							<Card.Body>
								<Card.Title>
									<h2>Angel's on Fire</h2>
								</Card.Title>
								<Card.Text>
										Super hot and spicy flavored pizza with jalapeno and sizzling chilli pepper.
								</Card.Text>
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</div>

		</>
	);

}