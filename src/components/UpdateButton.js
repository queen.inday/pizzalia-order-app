import React, { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import { Modal, Form, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

export default function UpdateButton({productId}) {

	const history = useHistory();

	const [ show, setShow ] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	// forms
	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ updateBtn, setUpdateBtn ] = useState(false);

	// save changes button conditional rendering
	useEffect(() => {

		if (name !== '' && description !== '' && price !== 0) {
			setUpdateBtn(true);

		} else {
			setUpdateBtn(false);
		}
	}, [ name, description, price ])


	// fetch update course from backend
	function updateProduct(e) {

		e.preventDefault();

		fetch(`https://still-escarpment-44325.herokuapp.com/products/update/${productId}`, {
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(update => {
			console.log(update);

			Swal.fire({
				title: `${name} information has been updated`,
				icon: 'success'
			}).then(() => history.go(0))
		});

		setName('');
		setDescription('');
		setPrice(0);

	}

	// UI for the Modal
	return (

		<>
			<Button variant="primary" className="update-btn" onClick={handleShow}>Update</Button>

			<Modal show={show} onHide={handleClose}>
				<Modal.Header closebutton="true">
					<Modal.Title>Update Product</Modal.Title>
				</Modal.Header>

				<Modal.Body>
					<Form onSubmit={(e) => updateProduct(e)}>
						<Form.Group>
							<Form.Label>Pizza Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="textarea" value={description} onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Form>
				</Modal.Body>

				<Modal.Footer>
					<Button variant="secondary" onClick={handleClose}>Discard</Button>

					{
						updateBtn ?
						<Button className="save-btn" type="submit" onClick={updateProduct}>Save Changes</Button>
						:
						<Button variant="secondary" type="submit" disabled>Save Changes</Button>
					}
					
					
				</Modal.Footer>

			</Modal>

		</>

	)
}
