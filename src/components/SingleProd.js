import React, { useContext, useState } from 'react';
import { Button, Jumbotron, Form } from 'react-bootstrap';
import UserContext from '../UserContext';

// add PropTypes in the Course component to validate its props
import PropTypes from 'prop-types';

export default function SingleProd({productInfo, productId}) {

	//const userContext = useContext(UserContext);
	const { user } = useContext(UserContext);
	const { name, description, price } = productInfo;

	return (
		
		<Jumbotron className="mt-5 mb-5 jumbo-single-prod">
			
			<h1 className="single-prod-title">{ name }</h1>

				<div>
					<span>{ description }</span>
				</div>
				<div className="mt-3"><p>Price: &#8369;
					<span>{ price }</span>
				</p></div>


			<Form className="mb-5">
			<Form.Label><strong>Quantity</strong></Form.Label>
		    <Form.Control type="number" className="input-number mb-4" placeholder="0" min="1" max="100" />
		   	</Form>

		    { prodBtn = user.accessToken === null 
		    	? 
		    	<Button className="mt-5" className="single-prod-btn" as={Link} to="/login">Sign in to Order</Button>
		    	: 
		    	<Button className="mt-5" className="single-prod-btn">Place Order</Button>
		    }
		</Jumbotron>
	)

}

// prototypes
SingleProd.propTypes = {

	productInfo: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}