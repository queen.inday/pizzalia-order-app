import React, { useContext } from 'react';
import { Navbar, Nav, Button } from 'react-bootstrap';
import logo from '../images/pizzalia.png';
import { NavLink, useHistory } from 'react-router-dom';
import UserContext from '../UserContext';

export default function NavBar() {

	const history = useHistory();

	const { user, setUser, unsetUser } = useContext(UserContext);
	// console.log(user);

	const logout = () => {
		unsetUser();
		setUser({ accessToken: null });
		history.push('/login')
	}

	let rightNav = (user.accessToken !== null) ?

		user.isAdmin ?
		<>

			<Nav.Link as={NavLink} to="/addProduct">Add a Product</Nav.Link>
			<Nav.Link as={NavLink} to="/allOrders">Orders</Nav.Link>
			<Nav.Link as={NavLink} to="/allUsers">Users</Nav.Link>
			<Nav.Link onClick={logout}>Logout</Nav.Link>

		</> :
		<>	
			<Nav.Link as={NavLink} to="/myAccount">My Account</Nav.Link>
			<Nav.Link as={NavLink} to="/myOrders">Orders</Nav.Link>
			<Nav.Link onClick={logout}>Logout</Nav.Link>
		</> : 
		<>
			<Nav.Link as={NavLink} to="/login"><Button style={{ backgroundColor: '#ffd166', color: 'black'}}>Sign In</Button></Nav.Link>
		     <Nav.Link as={NavLink} to="/register"><Button variant="secondary">Create Account</Button></Nav.Link>
		</>

	return (

		<Navbar variant="dark" className="nav-color">
		  <Navbar.Brand href="/">
	      <img alt="logo" src={ logo } height="50" className="d-inline-block align-top" />
		</Navbar.Brand>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />

		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="mr-auto nav-item">
		      <Nav.Link as={NavLink} to="/">Home</Nav.Link>
		      <Nav.Link as={NavLink} to="/products">Menu</Nav.Link>
		    </Nav>

		    <Nav className="ml-auto nav-item">
		      { rightNav }
		    </Nav>

		  </Navbar.Collapse>
		</Navbar>
	)
}
