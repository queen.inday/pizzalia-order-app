import React from 'react';
import { Carousel } from 'react-bootstrap';
import pizzaone from '../images/pizzaone.jpg';
import pizzatwo from '../images/pizzatwo.jpg';
import pizzathree from '../images/pizzathree.jpg';

export default function CarouselSlide() {

	return (

		<Carousel className="mt-5">
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src={ pizzaone }
		      alt="First slide"
		    />
		  </Carousel.Item>

		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src={ pizzatwo }
		      alt="Second slide"
		    />
		  </Carousel.Item>

		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src={ pizzathree }
		      alt="Third slide"
		    />
		  </Carousel.Item>
		</Carousel>
	)
}
